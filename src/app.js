const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

const routes = require('./routes');
const context = require('./env-config').context;
const logger = require('./utils/logger');

const app = express();

app.use(
  bodyparser.json({ limit: '5mb', type: ['application/json', 'text/plain'] })
);
app.use(bodyparser.urlencoded({ limit: '5mb', extended: true }));

app.use(cors());

// health check MS
app.get(`/${context.baseURI}/health`, (req, res) => {
  const content = { code: 'OK', message: `${context.name} up and running` };
  logger.info(content.message);

  res.status(200).json(content);
});


app.use(`/${context.baseURI}`, routes);

app.use((req, res) => {
  const content = { message: `Route ${req.url} Not found.`, code: 'NOT_FOUND' };
  logger.info(content.message);

  return res.status(404).json(content);
});


// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  const ErrorController = require('./controller/errorController');
  const errorController = new ErrorController();
  // eslint-disable-next-line prefer-const
  let { err, statusCode } = errorController.handle(error);
  logger.info({ msg: err.message, code: err.code });

  return res.status(statusCode).json(err);
});


module.exports = app;
