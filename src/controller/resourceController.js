const AbstractController = require('./abstractController');
const {
  retrievePhotos
} = require('./../services/photoService');
const {
  retrievePosts
} = require('./../services/postsService');

const CACHED_RESOURCES = {
  photos: [],
  posts: []
};

module.exports = class ResourceController extends AbstractController {

  paginate(limit, offset, resource = []) {
    limit = parseInt(limit);
    offset = parseInt(offset);
    const realLimit = limit > 50? 50: limit;
    const pages = Math.ceil(resource.length / realLimit) - 1;
    const realOffset = offset > pages ? pages: offset;
    const startPosition = realOffset * limit;
    const _endPosition = parseInt(startPosition) + parseInt(limit);
    const endPosition = _endPosition > resource.length? resource.length : _endPosition;
    
    return {
      records:resource.slice(startPosition, endPosition),
      total: pages, offset: realOffset
    };
  }

  async getPhotos(limit, offset) {
    // eslint-disable-next-line init-declarations
    let res;

    if (CACHED_RESOURCES.photos.length === 0) {
      res = await retrievePhotos();
      CACHED_RESOURCES.photos = res;
    }

    res = this.paginate(limit, offset, CACHED_RESOURCES.photos);

    return super.setResult(res);
  }

  async getPosts(limit, offset) {
    // eslint-disable-next-line init-declarations
    let res;

    if (CACHED_RESOURCES.posts.length === 0) {
      res = await retrievePosts();
      CACHED_RESOURCES.posts = res;
    }
    res = this.paginate(limit, offset, CACHED_RESOURCES.posts);

    return super.setResult(res);
  }
};
