module.exports = class AbstractController {
  setResult (data, status){
    return {
      data, status: status || {
        message: 'Request processed successfully',
        code: 'REQUEST_SUCCESSFULLY'
      }
    };
  }
};
