const { config } = require('./../env-config');
const ErrorEnums = require('./../utils/errorEnum');
const ErrorController = require('./errorController');
const AbstractController = require('./abstractController');
const generateCredentialService = require('./../services/credentialService').generateCredential;
const revokeCredentialService = require('./../services/credentialService').revokeCredential;
const validateCredentialService = require('./../services/credentialService').validateCredential;

module.exports = class CredentialsController extends AbstractController {
  constructor() {
    super();
    this.errorController = new ErrorController();
  }

  async generateCredentials(user, pass) {
    if (user === config.user.username && pass === config.user.password)
      return await generateCredentialService({ user });

    throw this.errorController.throw(ErrorEnums.INVALID_CREDENTIALS);
  }

  async validateCredentials(token) {
    try {
      return await validateCredentialService(token);
    } catch (e) {
      throw this.errorController.throw(e);
    }
  }

  async removeCredentials(token) {
    try {
      return await revokeCredentialService(token);
    } catch (e) {
      throw this.errorController.throw(e);
    }
  }

};
