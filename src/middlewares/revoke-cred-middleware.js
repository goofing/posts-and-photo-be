module.exports = async (req, res, next) => {
  try {
    req.result = req.credentialsController.removeCredentials(req.body)
    return next();
  } catch (e) {
    return next(e)
  }
};
