module.exports = async (req, res, next) => {

  const _limit = req.query.limit || 10;
  const _offset = req.query.offset || 0;
  req.limit = _limit;
  req.offset = _offset;

  return next();
};
