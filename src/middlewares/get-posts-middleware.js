const logger = require('./../utils/logger');

module.exports = async (req, res, next) => {
  try {
    logger.info('passing by get post middleware');
    req.result = await req.resourceController.getPosts(req.limit, req.offset);

    return next();
  } catch (e) {
    return next(e);
  }
};
