const ResourceController = require('../controller/resourceController');

module.exports = async (req, res, next) => {
  req.resourceController = new ResourceController();
  req.result = {};
  
  return next();
};
