module.exports = async (req, res, next) => {
  req.token = req.headers['authorization'] && req.headers['authorization'].split(' ').length > 1?
    req.headers['authorization'].split(' ')[1]:'';

  return next();
};
