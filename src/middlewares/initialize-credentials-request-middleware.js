const CredentialsController = require('../controller/credentialsController');

module.exports = async (req, res, next) => {
  req.credentialsController = new CredentialsController();
  req.result = {};
  
  return next();
};
