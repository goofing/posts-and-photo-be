module.exports = async (req, res, next) => {
  try {
    req.result = await req.credentialsController.generateCredentials(req.body.username, req.body.password);

    return next();
  } catch (e) {
    return next(e);
  }
};
