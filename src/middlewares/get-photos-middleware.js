const logger = require('./../utils/logger');

module.exports = async (req, res, next) => {
  try {
    logger.info('passing by get photos middleware');
    req.result = await req.resourceController.getPhotos(req.limit, req.offset);

    return next();
  } catch (e) {
    return next(e);
  }
};
