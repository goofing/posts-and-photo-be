module.exports = async (req, res, next) => {
  try {
    req.result = await req.credentialsController.validateCredentials(req.token);

    return next();
  } catch (e) {
    return next(e);
  }
};
