module.exports = {
  get_posts: [
    'set-token-on-request-middleware',
    'initialize-credentials-request-middleware',
    'validate-token-middleware',
    'set-pagination-to-req-middleware',
    'initialize-resource-request-middleware',
    'get-posts-middleware',
    'resource-response-middleware'
  ],
  get_photos: [
    'set-token-on-request-middleware',
    'initialize-credentials-request-middleware',
    'validate-token-middleware',
    'set-pagination-to-req-middleware',
    'initialize-resource-request-middleware',
    'get-photos-middleware',
    'resource-response-middleware'
  ],
  authentication: [
    'initialize-credentials-request-middleware',
    'authenticate-user-middleware',
    'credentials-response-middleware'
  ],
  revoke_credentials: [
    'set-token-on-request-middleware',
    'initialize-credentials-request-middleware',
    'validate-token-middleware',
    'revoke-cred-middleware',
    'credentials-response-middleware'
  ],
};
