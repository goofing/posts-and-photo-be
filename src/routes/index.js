const express = require('express');
const middlewareSpecifications = require('./middlewares-specifications')
  .definitions;
const { getMiddlewares } = require('./middlewares-specifications');
const router = express.Router();

router.post('/authenticate', getMiddlewares(middlewareSpecifications.authentication));
router.post('/revoke-credentials', getMiddlewares(middlewareSpecifications.revoke_credentials));
router.get('/post', getMiddlewares(middlewareSpecifications.get_posts));
router.get('/photo', getMiddlewares(middlewareSpecifications.get_photos));

module.exports = router;