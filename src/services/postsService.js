const { config } = require('./../env-config');
const HttpBuilder = require('./../utils/httpBuilder');
const httpBuilder = new HttpBuilder();

module.exports = {
  retrievePosts : async () => {
    const url = `${config.resources.url}/posts`;
    const request = httpBuilder.get(url);
    const response = await request.execute();

    return response;
  },
};