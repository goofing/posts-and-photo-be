const { config } = require('./../env-config');
const HttpBuilder = require('./../utils/httpBuilder');
const httpBuilder = new HttpBuilder();

module.exports = {
  generateCredential: async (data) => {
    const url = `${config.jwtApi.url}/jwt`;
    const request = httpBuilder.post(url, data);
    const response = await request.execute();

    return response;
  },
  validateCredential: async (token) => {
    const url = `${config.jwtApi.url}/verify`;
    const request = httpBuilder.post(url, { token });
    const response = await request.execute();

    return response;
  },
  revokeCredential: async (data) => {
    const url = `${config.jwtApi.url}/posts`;
    const request = httpBuilder.post(url, data);
    const response = await request.execute();

    return response;
  }
};