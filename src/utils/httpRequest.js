const axios = require('axios');


module.exports = function() {
  this.prepare = (options) => ({
    execute: async () => { // eslint-disable-next-line no-useless-catch
      try {
        const response = await axios(options);

        return response.data;
      } catch (e) {
        throw e.response.data;
      }
    }
  });
};
