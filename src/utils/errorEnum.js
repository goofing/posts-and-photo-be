const enums = {
  INVALID_CREDENTIALS: { code:'INVALID_CREDENTIALS', message:'Invalid credentials', statusCode: 400 },
  MALFORMED_REQUEST: { code:'MALFORMED_REQUEST', message:'Malformed body', statusCode: 400 },
  SOMETHING_WENT_WRONG: { code:'SOMETHING_WENT_WRONG', message:'Something went wrong' },
  WRONG_CREDENTIALS: { code:'WRONG_CREDENTIALS', message:'Wrong credentials' },

  //Mapped external Errors
  REFRESH_TOKEN_NOT_FOUND: { code:'INVALID_CREDENTIALS', message:'Invalid credentials', statusCode: 401 },
  INVALID_REFRESH_TOKEN: { code:'INVALID_CREDENTIALS', message:'Invalid credentials', statusCode: 401 },
  INVALID_TOKEN: { code:'INVALID_CREDENTIALS', message:'Invalid credentials', statusCode: 401 },
};

module.exports = enums;