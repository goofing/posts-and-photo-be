module.exports = {
  resources: {
    url: 'https://jsonplaceholder.typicode.com'
  },
  user: {
    username: process.env.USER || 'test@tes.com',
    first_name: process.env.FIRST_NAME || 'test',
    last_name: process.env.LAST_NAME || 'test',
    password: process.env.USER_PASS || 'admin'
  }
};
