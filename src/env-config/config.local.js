module.exports = {
  resources: {
    url: 'https://jsonplaceholder.typicode.com'
  },
  jwtApi: {
    url: process.env.JWT_API || 'http://localhost:8081/jwt-test'
  },
  user: {
    username: process.env.USER,
    first_name: process.env.FIRST_NAME || 'test',
    last_name: process.env.LAST_NAME || 'test',
    password: process.env.USER_PASS || 'admin'
  }
};
