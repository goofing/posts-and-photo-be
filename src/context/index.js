const context = {
  name: process.env.NODE_CONTEXT_NAME || 'core-test-service',
  port: process.env.SERVER_PORT || '8080',
  version: process.env.NODE_CONTEXT_VERSION || 'v1',
  baseURI: process.env.NODE_CONTEXT_BASE_URI || 'core-test',
};
module.exports = context;
