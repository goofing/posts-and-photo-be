# CORE-TEST

## Installation

1. Install node v12
   `nvm instal 12`

2. Clone this repository 😊
   `git clone ....`

3. Install dependencies
   `npm install`

4. Run project on develop mode
   `NODE_ENV=local USER=root npm run start`

## Considerations

This api needs jwt-api that is on repository **posts-and-photo-jwt-api**
